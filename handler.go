package tangles

import (
	"io"
	"mime/multipart"

	"github.com/pkg/errors"
)

// ErrNoSHA is returned when no SHA assertion can be read from
// a multipart form data part. Assertions are required, as blobs
// will be named and deduplicated based on assertions.
var ErrNoSHA = errors.New("no sha set as form name or filename")

// IncomingBlob represents an incoming blob. It holds the asserted
// SHA256 of the blob, and the reader that the blob may be obtained
// from.
type IncomingBlob struct {
	SHA256      string
	Reader      io.Reader
	ContentType string
}

// NextBlobFromMultipart returns an IncomingBlob by parsing the next
// part of the passed multipart.Reader. The IncomingBlob's SHA assertion
// should be set as the part's form name or filename. If no SHA assertion
// can be found, an ErrNoSHA error will be returned.
func NextBlobFromMultipart(reader *multipart.Reader) (IncomingBlob, error) {
	part, err := reader.NextPart()
	if err != nil {
		return IncomingBlob{}, errors.Wrap(err, "error parsing next part")
	}
	sha := part.FormName()
	if sha == "" {
		sha = part.FileName()
	}
	if sha == "" {
		return IncomingBlob{}, ErrNoSHA
	}
	return IncomingBlob{
		SHA256:      sha,
		ContentType: part.Header.Get("Content-Type"),
		Reader:      part,
	}, nil
}
