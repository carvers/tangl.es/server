package tangles

import (
	"bytes"
	"strings"
	"testing"
)

var streamingBlobUploadResultTests = map[string]Blob{
	`The quick brown fox jumped over the lazy dog.`: Blob{
		SHA256:      "40d5c6f7fe5672fb52269f651c2b985867dfcfa4a5c5258e3d4f8736d5095037",
		Size:        45,
		ContentType: "text/plain",
	},
}

func TestStreamingBlobUploadResult(t *testing.T) {
	for input, expectation := range streamingBlobUploadResultTests {
		dst := &bytes.Buffer{}
		result, err := StreamingBlobUpload(expectation.ContentType, strings.NewReader(input), dst)
		if err != nil {
			t.Fatalf("Error uploading streaming blob `%s`: %s", input, err)
		}
		if result.SHA256 != expectation.SHA256 {
			t.Errorf("Expected blob sha1 to be `%s`, got `%s`", expectation.SHA256, result.SHA256)
		}
		if result.Size != expectation.Size {
			t.Errorf("Expected blob size to be %d, got %d", expectation.Size, result.Size)
		}

		dstString := dst.String()
		if dstString != input {
			t.Errorf("Expected to write `%s`, wrote `%s`", input, dstString)
		}
	}
}
